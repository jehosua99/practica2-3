#include <iostream>

using namespace std;
/*
 El usuario debera ingresar dos palabras y el tamaño de ellas, por razones logicas si su tamaño es distinto las palabras son
 necesariamente distintas; programa le dira si las palabras a comparar son iguales.
*/

int main()
{
    short int caracteres, valorlogico = 1;
    cout << "Ingrese la cantidad de caracteres de las palabras:\n";  //es necesario saber que espacio de memoria se debe reservar
    cin >> caracteres;
    char palabra1[caracteres], palabra2[caracteres];   //reservare para las palabras el tamaño que ha sido ingresado
    cout << "Ingrese la palabra 1 a comparar: \n";
    cin >> palabra1;
    cout << "Ingrese la palabra 2 a comparar: \n";
    cin >> palabra2;
    for (int i = 0; i < caracteres; i++)  //recorro el arreglo
    {
        if (palabra1[i] != palabra2[i])  // si las palabras son iguales en todo momento no debera ingresar
        {
            valorlogico = 0;  //de lo contrario el valor logico cambiara y de inmediato interrumpira el ciclo puesto que ya se sabe que son distintas
            break;
        }
    }
    if (valorlogico == 0)
        cout << "Las palabras " << palabra1 << " y " << palabra2 << " son distintas\n";
    else
        cout << "Las palabras " << palabra1 << " y " << palabra2 << " son iguales\n";
    return 0;
}
